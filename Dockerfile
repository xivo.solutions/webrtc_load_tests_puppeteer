FROM xivoxc/alpine-puppeteer

EXPOSE 9222

WORKDIR /puppeteer-controller/
COPY puppeteer-controller .

CMD ./setup.sh
