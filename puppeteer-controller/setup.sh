#!/bin/bash 

npm install
npm run compile
mkdir -p node_modules/puppeteer/.local-chromium/
mv /chrome-linux node_modules/puppeteer/.local-chromium/
npx jasmine init
wget -qO- https://raw.githubusercontent.com/eficode/wait-for/master/wait-for | sh -s -- ${LOADTEST_URL}:${WEBPACK_PORT} -t 180 -- npx jasmine ./spec/scenarioRunner.js