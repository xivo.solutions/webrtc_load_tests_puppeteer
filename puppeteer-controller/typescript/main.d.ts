export interface WebRTCUser {
    'username': string,
    'userNumber': string,
    'userType': string,
    'partyNumber'?: string,
}

export interface WebRTCCall {
    'caller': WebRTCUser,
    'callee': WebRTCUser,
}