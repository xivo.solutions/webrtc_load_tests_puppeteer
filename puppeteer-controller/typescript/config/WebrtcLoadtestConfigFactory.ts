import { WebRTCCall, WebRTCUser } from '../main';

/**
 * Webrtc loadtest config contains webrtc call objects, 
 * each object contains a caller, a callee and call parameters.
 * @param startingNumber 
 * @param totalUsers 
 * @returns 
 */
export function callsBetweenUsersConfig(startingNumber: number, totalUsers: number): WebRTCCall[] {
    let config: WebRTCCall[] = [];
    let callerTemp: WebRTCUser;
    for (let i = 0; i < totalUsers; i++) {
        if (i % 2 == 1) {
            config.push({
                callee: incrementCalleeCredentials(startingNumber, i),
                caller: callerTemp
            });
            continue;
        }
        callerTemp = incrementCallerCredentials(startingNumber, i);
    };
    return config;
}

export function calleesConfig(startingNumber: number, totalUsers: number): WebRTCCall[] {
    let config: WebRTCCall[] = [];
    for (let i = 0; i < totalUsers; i++) {
        config.push({
            callee: incrementCalleeCredentials(startingNumber, i),
            caller: undefined
        });
    };
    return config;
}

/**
 * Webrtc loadtest config contains webrtc call objects, which have callers that are set to call against
 * a common peer, which is registered as a user
 * @param commonPeerUsername 
 * @param commonPeerNumber 
 * @param firstCallerNumber 
 * @param numberOfCallers 
 * @returns 
 */
export function callsAgainstCommonPeerConfig(commonPeerUsername: string, commonPeerNumber: string, firstCallerNumber: number, numberOfCallers: number): WebRTCCall[] {
    const commonPeerCredentials: WebRTCUser = generateCredentials(
        commonPeerUsername,
        commonPeerNumber,
        'callee'
    );
    const webrtcLoadtestInstance: WebRTCCall[] = initWebrtcCommonPeerLoadTestConfig(commonPeerCredentials, firstCallerNumber, numberOfCallers)

    return webrtcLoadtestInstance;
}

export function callsBetweenCommonPeersConfig(calleeCommonPeerName: string, calleeCommonPeerNumber: string, callerCommonPeerName: string, callerCommonPeerNumber: string): WebRTCCall[] {
    let config: WebRTCCall[] = [];
    const calleeCommonPeerCredentials: WebRTCUser = generateCredentials(
        calleeCommonPeerName,
        calleeCommonPeerNumber,
        'callee'
    );
    const callerCommonPeerCredentials: WebRTCUser = generateCredentials(
        callerCommonPeerName,
        callerCommonPeerNumber,
        'caller',
        calleeCommonPeerNumber,
    );
    config.push({
        callee: calleeCommonPeerCredentials,
        caller: callerCommonPeerCredentials,
    });
    return config;
}

function incrementCalleeCredentials(startingNumber: number, index: number): WebRTCUser {
    return generateCredentials(
        (startingNumber + index).toString(),
        (startingNumber + index).toString(),
        'callee',
        (startingNumber + index - 1).toString(),
    );
}

function incrementCallerCredentials(startingNumber: number, index: number): WebRTCUser {
    return generateCredentials(
        (startingNumber + index).toString(),
        (startingNumber + index).toString(),
        'caller',
        (startingNumber + index + 1).toString(),
    );
};

function initWebrtcCommonPeerLoadTestConfig(commonPeerCredentials: WebRTCUser, firstCallerNumber: number, numberOfCallers: number): WebRTCCall[] {
    let config: WebRTCCall[] = [];
    for (let i = 0; i < numberOfCallers; i++) {
        let callerCredentials: WebRTCUser = generateCredentials(
            (firstCallerNumber + i).toString(),
            (firstCallerNumber + i).toString(),
            'caller',
            commonPeerCredentials.userNumber
        );
        config.push({
            callee: commonPeerCredentials,
            caller: callerCredentials,
        });
    }
    return config;
}

function generateCredentials(username: string, number: string, type: string, partyNumber?: string): WebRTCUser {
    return {
        username: username,
        userNumber: number,
        userType: type,
        partyNumber: partyNumber,
    };
};