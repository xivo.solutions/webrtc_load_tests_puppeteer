const fs = require('fs');
// const puppeteerToIstanbul = require('puppeteer-to-istanbul');

const SCENARIO = process.env.SCENARIO;
const SCENARIO_CONDITION = process.env.SCENARIO_CONDITION;
const STARTING_PHONE_NUMBER = parseInt(process.env.STARTING_PHONE_NUMBER);
const DELAY = parseInt(process.env.DELAY);
const NUMBER_OF_USERS = parseInt(process.env.NUMBER_OF_USERS);
const AVERAGE_TIME_TO_CALL_MS = parseInt(process.env.AVERAGE_TIME_TO_CALL_MS);
const CALL_LENGTH_MS = parseInt(process.env.CALL_LENGTH_MS);

let CURRENT_ITERATION = 0;

const setCurrentIteration = (iteration) => {
    CURRENT_ITERATION = iteration;
}

const getCurrentIteration = () => {
    return CURRENT_ITERATION;
}

const getScenarioName = () => {
    return SCENARIO
}

const getScenarioDescription = () => {
    switch (String(SCENARIO)) {
        case "multiple_users":
            return 'test webrtc call and then hangup';
        case "multiple_calls":
            return 'test webrtc call and then hangup';
        default:
            console.log('Unknown scenario.');
    }
}

const delay = async () => {
    return new Promise(resolve => setTimeout(resolve, DELAY));
};

const startProfiling = async (page, userType, index) => {
    await page.tracing.start({ path: `./trace-${userType}-${index}.json` });
}

const stopProfiling = async (page) => {
    await page.tracing.stop();
}

const startJSCoverage = async (page) => {
    await page.coverage.startJSCoverage();
}

const stopJSCoverage = async (page) => {
    return await page.coverage.stopJSCoverage();
}

// const exportJSCoverageIstanbul = async (JSCoverage, index) => {
//     puppeteerToIstanbul.write([...JSCoverage], { includeHostname: true, storagePath: `./.nyc_output-${index}` })
// }

const exportJSCoverage = async (JSCoverage, index) => {
    console.log(`exportJSCoverage.parameter.JSCoverage > JSON.stringify(): ${JSON.stringify(JSCoverage)}`);
    console.log(`exportJSCoverage.parameter.JSCoverage > toString(): ${JSCoverage.toString()}`);
    const coverage = [...JSCoverage];
    console.log(`exportJSCoverage.localVar.coverage > JSON.stringify(): ${JSON.stringify(coverage)}`);
    console.log(`exportJSCoverage.localVar.coverage > toString(): ${coverage.toString()}`);
    for (const entry of coverage) {
        console.log(`exportJSCoverage.localVar.entry > JSON.stringify(): ${JSON.stringify(entry)}`);
        console.log(`exportJSCoverage.localVar.entry > toString(): ${entry.toString()}`);
        // Init covered_js
        let covered_js = "// JS Coverage: \n// ************************************ \n\n";
        // Check if the js asset contains '.js' in the URL, if it does then export the coverage JS & Ignoring inline scripts in the web page
        if (entry.url.search("\\.js") !== -1) {
            for (const range of entry.ranges) {
                // Iterate through the .json ranges
                covered_js += entry.text.slice(range.start, range.end) + "\n";
            }
            // Create and export each coverage JS file
            const filePath = `./${exportJSFileName(entry.url)}-${index}`
            fs.writeFile(filePath, covered_js, function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log(`The file ${filePath} was saved!`);
            });
        }

    }
}

const exportJSFileName = (url) => {
    // Remove https:// from the beginning of the string & .js* from the end of the string
    var newFileName = url.substring(8, url.search("\\.js"));
    // Replace all forward slashes with hyphens
    newFileName = newFileName.replace(/\//g, "-");
    // Return filename with .js concatenated at the end    
    return newFileName + ".js";
}

const setIterationTimeout = () => {
    return (NUMBER_OF_USERS * AVERAGE_TIME_TO_CALL_MS * 4 + CALL_LENGTH_MS);
}

const getNthNumberFromTheEnd = (position) => {
    if (position <= 0) {
        console.error('getNthNumberFromTheEnd: ERROR - the parameter should be > 0.');
        return;
    }
    let nthNumberFromTheEnd = `${STARTING_PHONE_NUMBER + NUMBER_OF_USERS - position}`;
    return nthNumberFromTheEnd;
}

const getTotalCallCount = () => {
    const totalCallCount = NUMBER_OF_USERS / 2;
    return totalCallCount;
}

const getLastCall = () => {
    let lastCall = `call-${getNthNumberFromTheEnd(2)}-${getNthNumberFromTheEnd(1)}`;
    return lastCall;
}

const getIterationParameters = () => {
    const scenarioName = SCENARIO;
    const numOfCalls = getTotalCallCount();
    const lastExpectedCall = getLastCall();
    const iteration = getCurrentIteration();
    const iterationParameters = {
        scenarioName: scenarioName,
        numOfUsers: NUMBER_OF_USERS,
        numOfCalls: numOfCalls,
        iteration: iteration,
        firstNumber: STARTING_PHONE_NUMBER,
        lastExpectedCall: lastExpectedCall,
        condition: SCENARIO_CONDITION,
    };
    const UrlSearchParams = new URLSearchParams(iterationParameters);
    const searchParams = UrlSearchParams.toString();
    return searchParams;
}

module.exports = {
    setCurrentIteration: setCurrentIteration,
    getCurrentIteration: getCurrentIteration,
    getScenarioName: getScenarioName,
    getScenarioDescription: getScenarioDescription,
    delay: delay,
    startProfiling: startProfiling,
    stopProfiling: stopProfiling,
    startJSCoverage: startJSCoverage,
    stopJSCoverage: stopJSCoverage,
    exportJSCoverage: exportJSCoverage,
    // exportJSCoverageIstanbul: exportJSCoverageIstanbul,
    setIterationTimeout, setIterationTimeout,
    getNthNumberFromTheEnd: getNthNumberFromTheEnd,
    getIterationParameters: getIterationParameters,
}