const http = require('http');
const { getIterationParameters } = require('./utils');

const LOADTEST_URL = process.env.LOADTEST_URL;
const CLIENT_PROXY_PORT = parseInt(process.env.CLIENT_PROXY_PORT);

const getWebrtcLoadtestReport = async () => {
    console.log(`ClientProxyManager: Fetching the load test report from client proxy.`);
    const url = buildQueryUrl('get');
    const httpOptions = httpOtionsFactory(url, 'POST');
    return resolveQuery(httpOptions);
}

const awaitScenarioIterationToEnd = async () => {
    console.log(`ClientProxyManager: Registering a callback to client proxy.`);
    const iterationParameters = getIterationParameters();
    const url = buildQueryUrl('setup', iterationParameters);
    const httpOptions = httpOtionsFactory(url, 'GET');
    return await resolveQuery(httpOptions);
}

const buildQueryUrl = (path, parameters = null) => {
    if (parameters) {
        return `/${path}?${parameters}`
    } else {
        return `/${path}`;
    }
}

const httpOtionsFactory = (url, method) => {
    return {
        hostname: LOADTEST_URL,
        port: CLIENT_PROXY_PORT,
        path: url,
        method: method
    };
}

const resolveQuery = async (httpOptions) => {
    return new Promise(
        (resolve, reject) => {
            let body = []
            let request = http.request(httpOptions, (res) => {
                console.log(`ClientProxyManager: Got response with body from client proxy.`);
                if (res.statusCode < 200 || res.statusCode >= 300) {
                    return reject(new Error('statusCode=' + res.statusCode));
                }

                res.on('data', (chunk) => {
                    body.push(chunk);
                }).on('end', () => {
                    try {
                        console.log(`ClientProxyManager: resolving response body.`);
                        const parsedBody = JSON.parse(Buffer.concat(body).toString());
                        resolve(parsedBody);
                    } catch (e) {
                        console.log(`ClientProxyManager: FAILED resolving response body.`);
                        reject(e);
                    }
                });
            });
            request.on('error', (err) => { reject(err) });
            request.end();
        })
}

module.exports = {
    getWebrtcLoadtestReport: getWebrtcLoadtestReport,
    awaitScenarioIterationToEnd: awaitScenarioIterationToEnd,
}