const {
    attachLogger,
    navigateToUrl,
    turnOffTimeout,
    reloadPage,
} = require('./puppeteerLib');
const { getNthNumberFromTheEnd } = require('./utils');

const STARTING_PHONE_NUMBER = process.env.STARTING_PHONE_NUMBER;
const CALL_LENGTH_MS = process.env.CALL_LENGTH_MS;
const DELAY_BETWEEN_CALLS = process.env.DELAY_BETWEEN_CALLS;
const CALLEE_COMMON_PEER_ID = process.env.CALLEE_COMMON_PEER_ID;
const CALLEE_COMMON_PEER_USERNAME = process.env.CALLEE_COMMON_PEER_USERNAME;
const CALLEE_COMMON_PEER_NUMBER = process.env.CALLEE_COMMON_PEER_NUMBER;
const CALLER_COMMON_PEER_ID = process.env.CALLER_COMMON_PEER_ID;
const CALLER_COMMON_PEER_USERNAME = process.env.CALLER_COMMON_PEER_USERNAME;
const CALLER_COMMON_PEER_NUMBER = process.env.CALLER_COMMON_PEER_NUMBER;
const LOADTEST_URL = process.env.LOADTEST_URL;
const WEBPACK_PORT = process.env.WEBPACK_PORT;
const SCENARIO = process.env.SCENARIO;

const loadUser = async (userTab, user, userType, userIndex) => {
    const scenarioUserType = `${SCENARIO}_${userType}`;
    const userUrl = buildUrlFor(scenarioUserType, user);
    console.log(`Loading user with url: ${userUrl}`);
    turnOffTimeout(userTab);
    attachLogger(userTab, userType, userIndex);
    await navigateToUrl(userTab, userUrl);
}

const loadUsersGradually = async (userTabs, webrtcLoadtestConfig, userType) => {
    for (let i = 0; i < userTabs.length; i++) {
        console.log(`Loading user ${userTabs[i]} ${webrtcLoadtestConfig[i][userType]} ${userType} ${i}`);
        if (webrtcLoadtestConfig[i][userType] !== undefined) {
            await loadUser(userTabs[i], webrtcLoadtestConfig[i][userType], userType, i)
        }
    }
}

const reloadUsersGradually = async (userTabs) => {
    for (let i = 0; i < userTabs.length; i++) {
        const userTab = userTabs[i];
        await reloadPage(userTab);
    }
}

const buildUrlFor = (scenarioUserType, user) => {
    switch (scenarioUserType) {
        case "multiple_users_callee":
        case "multiple_agents_callee":
            return buildUserUrlForMultipleUsersCallee(user);
        case "multiple_users_caller":
            return buildUserUrlForMultipleUsersCaller(user);
        case "multiple_calls_callee":
            return buildUserUrlForMultipleCallsCallee(user);
        case "multiple_calls_caller":
            return buildUserUrlForMultipleCallsCaller(user)
        case "multiple_agents_caller":
            console.log('skipping agents callers');
        default:
            console.log(`Unknown scenario user type: ${scenarioUserType}`);
    }
}

const buildUserUrlForMultipleUsersCallee = (user) => {
    const userType = user.userType.charAt(0).toUpperCase() + user.userType.slice(1);
    let url = `http://${LOADTEST_URL}:${WEBPACK_PORT}/${SCENARIO}${userType}.html`
    url = url.concat(`?username=${user.username}`);
    url = url.concat(`&userType=${user.userType}`);
    url = url.concat(`&userNumber=${user.userNumber}`);
    url = url.concat(`&partyNumber=${user.partyNumber}`);
    url = url.concat(`&callLength=${CALL_LENGTH_MS}`);
    url = includePasswordIfPresent(url, user);
    return url;
};

const buildUserUrlForMultipleUsersCaller = (user) => {
    const userType = user.userType.charAt(0).toUpperCase() + user.userType.slice(1);
    let url = `http://${LOADTEST_URL}:${WEBPACK_PORT}/${SCENARIO}${userType}.html`
    url = url.concat(`?username=${user.username}`);
    url = url.concat(`&userType=${user.userType}`);
    url = url.concat(`&userNumber=${user.userNumber}`);
    url = url.concat(`&partyNumber=${user.partyNumber}`);
    url = url.concat(`&firstNumber=${STARTING_PHONE_NUMBER}`);
    url = url.concat(`&lastNumber=${getNthNumberFromTheEnd(1)}`);
    url = url.concat(`&delayBetweenCalls=${DELAY_BETWEEN_CALLS}`);
    url = includePasswordIfPresent(url, user);
    return url;
};

const buildUserUrlForMultipleCallsCallee = (user) => {
    const userType = user.userType.charAt(0).toUpperCase() + user.userType.slice(1);
    let url = `http://${LOADTEST_URL}:${WEBPACK_PORT}/${SCENARIO}${userType}.html`
    url = url.concat(`?username=${user.username}`);
    url = url.concat(`&userType=${user.userType}`);
    url = url.concat(`&userNumber=${user.userNumber}`);
    url = url.concat(`&partyNumber=${user.partyNumber}`);
    url = url.concat(`&calleeCommonPeerId=${CALLEE_COMMON_PEER_ID}`);
    url = url.concat(`&calleeCommonPeerName=${CALLEE_COMMON_PEER_USERNAME}`);
    url = url.concat(`&calleeCommonPeerNumber=${CALLEE_COMMON_PEER_NUMBER}`);
    url = includePasswordIfPresent(url, user);
    return url;
};

const buildUserUrlForMultipleCallsCaller = (user) => {
    const userType = user.userType.charAt(0).toUpperCase() + user.userType.slice(1);
    let url = `http://${LOADTEST_URL}:${WEBPACK_PORT}/${SCENARIO}${userType}.html`
    url = url.concat(`?username=${user.username}`);
    url = url.concat(`&userType=${user.userType}`);
    url = url.concat(`&userNumber=${user.userNumber}`);
    url = url.concat(`&partyNumber=${user.partyNumber}`);
    url = url.concat(`&callerCommonPeerId=${CALLER_COMMON_PEER_ID}`);
    url = url.concat(`&callerCommonPeerName=${CALLER_COMMON_PEER_USERNAME}`);
    url = url.concat(`&callerCommonPeerNumber=${CALLER_COMMON_PEER_NUMBER}`);
    url = url.concat(`&delayBetweenCalls=${DELAY_BETWEEN_CALLS}`);
    url = includePasswordIfPresent(url, user);
    return url;
};

const includePasswordIfPresent = (url, userCredentials) => {
    const userPassword = userCredentials.password;
    if (userPassword != undefined) {
        url = url.concat(`$password=${userPassword}`);
        return url;
    } else {
        return url;
    }
}

const interceptRequests = async (page) => {
    await page.setRequestInterception(true);
    page.on('request', interceptedRequest => {
        if (interceptedRequest.isInterceptResolutionHandled()) return;
        console.log(`interceptedRequest.frame(): ${JSON.stringify(interceptedRequest.frame())}`);
        console.log(`interceptedRequest.url(): ${JSON.stringify(interceptedRequest.url())}`);
        console.log(`interceptedRequest.initiator(): ${JSON.stringify(interceptedRequest.initiator())}`);
        console.log(`interceptedRequest.method(): ${JSON.stringify(interceptedRequest.method())}`);
        console.log(`interceptedRequest.headers(): ${JSON.stringify(interceptedRequest.headers())}`);
        console.log(`interceptedRequest.postData(): ${JSON.stringify(interceptedRequest.postData())}`);
        interceptedRequest.continue();
    });
}

module.exports = {
    loadUsersGradually: loadUsersGradually,
    reloadUsersGradually: reloadUsersGradually,
    loadUser: loadUser,
}