const { loadUsersGradually, reloadUsersGradually } = require('./sessionManager');
const { callsBetweenUsersConfig, callsBetweenCommonPeersConfig, calleesConfig } = require('./WebrtcLoadtestConfigFactory');
const { delay, getCurrentIteration } = require('./utils');
const { awaitScenarioIterationToEnd } = require('./clientProxyManager');
const { closeAllBrowsers, getCallersTabs, getCalleesTabs, initBrowsers } = require('./browserManager');

const SCENARIO_NAME = process.env.SCENARIO;
const BROWSER_MODE = parseInt(process.env.BROWSER_MODE);
const STARTING_PHONE_NUMBER = parseInt(process.env.STARTING_PHONE_NUMBER);
const NUMBER_OF_USERS = parseInt(process.env.NUMBER_OF_USERS);
const CALLEE_COMMON_PEER_NUMBER = process.env.CALLEE_COMMON_PEER_NUMBER;
const CALLEE_COMMON_PEER_USERNAME = process.env.CALLEE_COMMON_PEER_USERNAME;
const CALLER_COMMON_PEER_NUMBER = process.env.CALLER_COMMON_PEER_NUMBER;
const CALLER_COMMON_PEER_USERNAME = process.env.CALLER_COMMON_PEER_USERNAME;

const getConfig = () => {
    switch (SCENARIO_NAME) {
        case "multiple_users":
            return callsBetweenUsersConfig(STARTING_PHONE_NUMBER, NUMBER_OF_USERS);
        case "multiple_agents":
            return calleesConfig(STARTING_PHONE_NUMBER, NUMBER_OF_USERS);
        case 'multiple_calls':
            return callsBetweenCommonPeersConfig(CALLEE_COMMON_PEER_USERNAME, CALLEE_COMMON_PEER_NUMBER, CALLER_COMMON_PEER_USERNAME, CALLER_COMMON_PEER_NUMBER);
        default:
            () => console.log('Unknown scenario.'); return;
    }
}

const loadUsers = async () => {
    const callersTabs = getCallersTabs();
    const calleesTabs = getCalleesTabs();
    await loadUsersGradually(callersTabs, getConfig(), 'caller');
    await loadUsersGradually(calleesTabs, getConfig(), 'callee');
    console.log('loaded all users');
}

const loadOnlyCallers = async () => {
    const callersTabs = getCallersTabs();
    await loadUsersGradually(callersTabs, getConfig(), 'caller');
    console.log('loaded all users');
}

const loadOnlyCallees = async () => {
    const calleesTabs = getCalleesTabs();
    await loadUsersGradually(calleesTabs, getConfig(), 'callee');
    console.log('loaded all users');
}

const reloadUsers = async () => {
    const callersTabs = getCallersTabs();
    const calleesTabs = getCalleesTabs();
    await reloadUsersGradually(calleesTabs);
    await reloadUsersGradually(callersTabs);
    console.log('reloaded all users');
    // const callersTabs = getCallersTabs();
    // await reloadUsersGradually(callersTabs);
    // (BROWSER_MODE !== 3) && await reloadUsersGradually(calleesTabs);
    // console.log('reloaded all users');
}

const reloadWebrtcLoad = async () => {
    await delay();
    await reloadUsers();
}

const initWebrtcLoad = async () => {
    await initBrowsers();
    switch (BROWSER_MODE) {
        case 0:
        case 1:
        case 2:
        case 4:
        case 5:
            await loadUsers();
        case 3:
            await loadOnlyCallers();
        case 6:
            await loadOnlyCallees();
    }
}

const tearDownEverything = async () => {
    await closeAllBrowsers();
    console.log('teared down everything');
}

const executeScenario = async () => {
    let actions = [];
    actions.push(awaitScenarioIterationToEnd());
    (getCurrentIteration() == 0) ? actions.push(initWebrtcLoad()) : actions.push(reloadWebrtcLoad());
    await Promise.all(actions);
}

module.exports = {
    tearDownEverything: tearDownEverything,
    executeScenario: executeScenario,
}