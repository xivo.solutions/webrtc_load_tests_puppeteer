const { getWebrtcLoadtestReport } = require('./clientProxyManager');
const { getCurrentIteration } = require('./utils');

// const SCENARIO = process.env.SCENARIO;

let WEBRTC_LOADTEST_REPORT;

const getReport = async () => {
    WEBRTC_LOADTEST_REPORT = await getWebrtcLoadtestReport();
}

const expectAllCallersIndisponible = () => {
    const result = agregateReport('caller', 'INDISPONIBLE', true);
    expect(result).toBeTruthy();
}

const expectAllCallersAvailable = () => {
    const result = agregateReport('caller', 'AVAILABLE', true);
    expect(result).toBeTruthy();
}

const expectAllCallersDialing = () => {
    const result = agregateReport('caller', 'EventDialing', true);
    expect(result).toBeTruthy();
}

const expectAllCallersEstablished = () => {
    const result = agregateReport('caller', 'EventEstablished', true);
    expect(result).toBeTruthy();
}

const expectAllCallersReleased = () => {
    const result = agregateReport('caller', 'EventReleased', true);
    expect(result).toBeTruthy();
}

const expectNoCallersDisconnected = () => {
    const result = agregateReport('caller', 'DISCONNECTED', false);
    expect(result).toBeTruthy();
}

const expectNoCallersRinging = () => {
    const result = agregateReport('caller', 'EventRinging', false);
    expect(result).toBeTruthy();
}

const expectNoCallersOnHold = () => {
    const result = agregateReport('caller', 'EventOnHold', false);
    expect(result).toBeTruthy();
}

const expectNoCallersFailure = () => {
    const result = agregateReport('caller', 'EventFailure', false);
    expect(result).toBeTruthy();
}

const expectNoCallersError = () => {
    const result = agregateReport('caller', 'ERROR', false);
    expect(result).toBeTruthy();
}

const expectNoCallersUnexisting = () => {
    const result = agregateReport('caller', 'UNEXISTING', false);
    expect(result).toBeTruthy();
}

const expectAllCalleesIndisponible = () => {
    const result = agregateReport('callee', 'INDISPONIBLE', true);
    expect(result).toBeTruthy();
}

const expectAllCalleesAvailable = () => {
    const result = agregateReport('callee', 'AVAILABLE', true);
    expect(result).toBeTruthy();
}

const expectAllCalleesRinging = () => {
    const result = agregateReport('callee', 'EventRinging', true);
    expect(result).toBeTruthy();
}

const expectAllCalleesEstablished = () => {
    const result = agregateReport('callee', 'EventEstablished', true);
    expect(result).toBeTruthy();
}

const expectAllCalleesReleased = () => {
    const result = agregateReport('callee', 'EventReleased', true);
    expect(result).toBeTruthy();
}

const expectNoCalleesDisconnected = () => {
    const result = agregateReport('callee', 'DISCONNECTED', false);
    expect(result).toBeTruthy();
}

const expectNoCalleesDialing = () => {
    const result = agregateReport('callee', 'EventDialing', false);
    expect(result).toBeTruthy();
}

const expectNoCalleesOnHold = () => {
    const result = agregateReport('callee', 'EventOnHold', false);
    expect(result).toBeTruthy();
}

const expectNoCalleesFailure = () => {
    const result = agregateReport('callee', 'EventFailure', false);
    expect(result).toBeTruthy();
}

const expectNoCalleesError = () => {
    const result = agregateReport('callee', 'ERROR', false);
    expect(result).toBeTruthy();
}

const expectNoCalleesUnexisting = () => {
    const result = agregateReport('callee', 'UNEXISTING', false);
    expect(result).toBeTruthy();
}

const agregateReport = (userType, metric, expectedValue) => {
    const iteration = `iteration.${getCurrentIteration()}`;
    const callsInIteration = Object.keys(WEBRTC_LOADTEST_REPORT[iteration]).filter(e => e.includes("call-"));
    const callEvent = `${userType}.${metric}`;
    return validateCalls(iteration, callsInIteration, callEvent, expectedValue);
}

const validateCalls = (iteration, callsInIteration, callEvent, expectedValue) => {
    let problematicCalls = [];
    const arr = callsInIteration.map(call => {
        const callReport = WEBRTC_LOADTEST_REPORT[iteration][call]
        const isEventInCall = callReport.some(ce => ce.includes(callEvent));
        (isEventInCall != expectedValue) && problematicCalls.push(call);
        return isEventInCall;
    });
    const result = arr.every(value => value === expectedValue);

    (problematicCalls.length > 0) && console.log("CallEvaluator: Problematic calls: ", ...problematicCalls);
    return result;
}

const showLoadtestReport = () => {
    console.log(`CallEvaluator: loadtest report: \n ${JSON.stringify(WEBRTC_LOADTEST_REPORT, null, 2)}`);
}

const evaluateRegistration = async () => {
    await getReport().then(() => {
        //caller positive tests
        expectAllCallersIndisponible();
        expectAllCallersAvailable();
        //caller negative tests
        expectNoCallersDisconnected();
        expectNoCallersRinging();
        expectNoCallersOnHold();
        expectNoCallersFailure();
        expectNoCallersError();
        expectNoCallersUnexisting();
        //callee positive tests
        expectAllCalleesIndisponible();
        expectAllCalleesAvailable();
        //callee negative tests
        expectNoCalleesDisconnected();
        expectNoCalleesDialing();
        expectNoCalleesOnHold();
        expectNoCalleesFailure();
        expectNoCalleesError();
        expectNoCalleesUnexisting();
    });
}

const evaluateCall = async () => {
    await getReport().then(() => {
        //caller positive tests
        // expectAllCallersIndisponible();
        expectAllCallersAvailable();
        expectAllCallersDialing();
        expectAllCallersEstablished();
        expectAllCallersReleased();
        //caller negative tests
        expectNoCallersDisconnected();
        expectNoCallersRinging();
        expectNoCallersOnHold();
        expectNoCallersFailure();
        expectNoCallersError();
        expectNoCallersUnexisting();
        //callee positive tests
        // expectAllCalleesIndisponible();
        expectAllCalleesAvailable();
        expectAllCalleesRinging();
        expectAllCalleesEstablished();
        expectAllCalleesReleased();
        //callee negative tests
        expectNoCalleesDisconnected();
        expectNoCalleesDialing();
        expectNoCalleesOnHold();
        expectNoCalleesFailure();
        expectNoCalleesError();
        expectNoCalleesUnexisting();
    });
}

// const EVALUATE_SCENARIO = {
//     "multiple_users": async () => evaluateCall(),
//     "multiple_calls": async () => evaluateCall(),
// }

const evaluateScenario = async () => {
    console.log('Evaluating executed scenario.')
    // await EVALUATE_SCENARIO[SCENARIO]();
    await evaluateCall();
}

module.exports = {
    getReport: getReport,
    showLoadtestReport: showLoadtestReport,
    evaluateScenario: evaluateScenario,
}