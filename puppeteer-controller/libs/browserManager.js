const {
    openContextsGradually,
    openTabsGradually,
    closeBrowsersDefaultPages,
    launchBrowser,
    openNumberOfContextsInBrowser,
    launchBrowsersGradually
} = require('./puppeteerLib');
const { delay } = require('./utils');

const BROWSER_MODE = parseInt(process.env.BROWSER_MODE);
const NUMBER_OF_USERS = parseInt(process.env.NUMBER_OF_USERS);

let browser;
let browserOctects = {}; // used for 8 user per browser 
let callersBrowsers;
let calleesBrowsers;
let callersContexts;
let calleesContexts;
let callersTabs;
let calleesTabs;

const browserForEachParty = async () => {
    callersBrowsers = await launchBrowser();
    calleesBrowsers = await launchBrowser();
    console.log('all browsers opened');
};

const oneBrowserPerUser = async () => {
    callersBrowsers = await launchBrowsersGradually(NUMBER_OF_USERS / 2);
    calleesBrowsers = await launchBrowsersGradually(NUMBER_OF_USERS / 2);
    console.log('all browsers opened');
};

const oneBrowserPerEightUsers = async () => {
    countOfBrowserOctects = NUMBER_OF_USERS / 8;
    for (let i = 0; i < countOfBrowserOctects; i++) {
        console.log(`opening browser num ${i + 1}`);
        browserOctects[i] = await launchBrowser();
        await delay();
    }
    console.log('all browsers opened');
}

const getAllBrowsers = () => {
    let allBrowsers = [];
    if (browser != undefined) {
        allBrowsers.push(browser);
    }
    if (callersBrowsers != undefined) {
        (!Array.isArray(callersBrowsers)) ? allBrowsers.push(callersBrowsers) : allBrowsers.push(...callersBrowsers);
    }
    if (calleesBrowsers != undefined) {
        (!Array.isArray(calleesBrowsers)) ? allBrowsers.push(calleesBrowsers) : allBrowsers.push(...calleesBrowsers);
    }
    if (Object.keys(browserOctects).length != 0) {
        Object.keys(browserOctects).forEach((browser) => {
            allBrowsers.push(browserOctects[browser]);
        });
    }
    return allBrowsers;
};

const oneContextPerBrowser = async () => {
    callersContexts = await openContextsGradually(callersBrowsers);
    calleesContexts = await openContextsGradually(calleesBrowsers);
    console.log('all incognito contexts opened');
};

const multipleContextsPerBrowser = async () => {
    callersContexts = await openNumberOfContextsInBrowser(callersBrowsers, NUMBER_OF_USERS / 2);
    calleesContexts = await openNumberOfContextsInBrowser(calleesBrowsers, NUMBER_OF_USERS / 2);
    console.log('all incognito contexts opened');
};

const oneContextOneCaleeAndOneContextPerCaller = async () => {
    callersContexts = await openNumberOfContextsInBrowser(callersBrowsers, NUMBER_OF_USERS);
    calleesContexts = await openNumberOfContextsInBrowser(calleesBrowsers, 1);
    console.log('all incognito contexts opened');
};

const oneContextPerUserInBrowserOctet = async () => {
    const boKeys = Object.keys(browserOctects);
    calleesContexts = [];
    callersContexts = [];
    for (let i = 0; i < boKeys.length; i++) {
        const browser = browserOctects[boKeys[i]];
        const calleesContextsTemp = await openNumberOfContextsInBrowser(browser, 4);
        const callersContextsTemp = await openNumberOfContextsInBrowser(browser, 4);
        calleesContexts.push(...calleesContextsTemp);
        callersContexts.push(...callersContextsTemp);
    }
    console.log('all contexts opened');
}

const oneTabPerContext = async () => {
    callersTabs = await openTabsGradually(callersContexts);
    calleesTabs = await openTabsGradually(calleesContexts);
    console.log('all tabs opened');
    const allBrowsers = getAllBrowsers();
    await closeBrowsersDefaultPages(allBrowsers);
};

const getCallersTabs = () => {
    return callersTabs;
};

const getCalleesTabs = () => {
    return calleesTabs;
};

const getAllTabs = () => {
    let allTabs = [];
    (callersTabs) && (!Array.isArray(callersTabs)) ? allTabs.push(callersTabs) : allTabs.push(...callersTabs);
    (calleesTabs) && (!Array.isArray(calleesTabs)) ? allTabs.push(calleesTabs) : allTabs.push(...calleesTabs);
    return allTabs;
};

const oneBrowserForAll = async () => {
    browser = await launchBrowser();
    console.log('all browsers opened');
    const count = (NUMBER_OF_USERS > 1) ? NUMBER_OF_USERS / 2 : NUMBER_OF_USERS;
    callersContexts = await openNumberOfContextsInBrowser(browser, count);
    calleesContexts = await openNumberOfContextsInBrowser(browser, count);
    console.log('all contexts opened')
    await oneTabPerContext();
};

const twoBrowsersOneContextPerUser = async () => {
    await browserForEachParty();
    await multipleContextsPerBrowser();
    await oneTabPerContext();
};

const twoBrowsersOneContextPerCaller = async () => {
    await browserForEachParty();
    await oneContextOneCaleeAndOneContextPerCaller();
    await oneTabPerContext();
};

const loadOnlyCallers = async () => {
    callersBrowsers = await launchBrowser();
    callersContexts = await openNumberOfContextsInBrowser(callersBrowsers, NUMBER_OF_USERS);
    callersTabs = await openTabsGradually(callersContexts);
    await closeBrowsersDefaultPages([callersBrowsers]);
};

const loadOnlyCallees = async () => {
    calleesBrowsers = await launchBrowser();
    calleesContexts = await openNumberOfContextsInBrowser(calleesBrowsers, NUMBER_OF_USERS);
    calleesTabs = await openTabsGradually(calleesContexts);
    await closeBrowsersDefaultPages([calleesBrowsers]);
};

const everyoneHasBrowser = async () => {
    await oneBrowserPerUser();
    await oneContextPerBrowser();
    await oneTabPerContext();
};

const oneBrowserForFourPairs = async () => {
    await oneBrowserPerEightUsers();
    await oneContextPerUserInBrowserOctet();
    await oneTabPerContext();
}

const init = {
    0: async () => await oneBrowserForAll(),
    1: async () => await twoBrowsersOneContextPerUser(),
    2: async () => await twoBrowsersOneContextPerCaller(),
    3: async () => await loadOnlyCallers(),
    4: async () => await everyoneHasBrowser(),
    5: async () => await oneBrowserForFourPairs(),
    6: async () => await loadOnlyCalees()
}

const initBrowsers = async () => {
    await init[BROWSER_MODE]()
}

const closeAllBrowsers = async () => {
    let allBrowsers = getAllBrowsers();
    for (let i = 0; i < allBrowsers.length; i++) {
        if (browser == undefined) return;
        console.log(`Closing browser no. ${i + 1}.`);
        await browser.close();
    }
}

const stopAllProfiling = async () => {
    const allTabs = getAllTabs();
    for (let i = 0; i < allTabs.length; i++) {
        console.log(`Stopping tracing on tab number: ${i}`);
        await stopProfiling(allTabs[i]);
    }
}

const stopAndExportAllJSCoverage = async () => {
    const allTabs = getAllTabs();
    for (let i = 0; i < allTabs.length; i++) {
        console.log(`Stopping tracing on tab number: ${i}`);
        const coverage = await stopJSCoverage(allTabs[i]);
        await exportJSCoverageIstanbul(coverage, i);
    }
}

module.exports = {
    getCallersTabs: getCallersTabs,
    getCalleesTabs: getCalleesTabs,
    initBrowsers: initBrowsers,
    closeAllBrowsers: closeAllBrowsers,
    stopAllProfiling: stopAllProfiling,
    stopAndExportAllJSCoverage: stopAndExportAllJSCoverage,
}