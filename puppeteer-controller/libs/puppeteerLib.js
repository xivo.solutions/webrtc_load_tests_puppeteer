const puppeteer = require('puppeteer');
const { delay } = require('./utils');

const DEBUG = (process.env.DEBUG == "true");
const DEBUG_PORT = process.env.DEBUG_PORT;

const launchBrowser = async () => {
    let argsArray = [
        '--use-fake-ui-for-media-stream',
        '--use-fake-device-for-media-stream',
        '--use-file-for-fake-audio-capture=./reno_project-system.wav',
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-accelerated-2d-canvas',
        '--disable-dev-shm-usage',
        '--disable-features=WebRtcHideLocalIpsWithMdns',
        '--enforce-webrtc-ip-permission-check',
        '--force-webrtc-ip-handling-policy',
        '--webrtc-stun-probe-trial',
        '--enable-webrtc-stun-origin',
        '--autoplay-policy=no-user-gesture-required',
        '--disable-background-networking',
        '--disable-component-extensions-with-background-pages',
        '--dns-prefetch-disable',
        '--disable-external-intent-requests',
        '--allow-running-insecure-content',
        '--autoplay-policy=user-gesture-required',
        // '--disable-gpu',
        // '--disable-gpu-compositing',
        // '--disable-gpu-driver-bug-workarounds',
        // '--disable-gpu-early-init',
        // '--disable-gpu-memory-buffer-compositor-resources',
        // '--disable-gpu-memory-buffer-video-frames',
        // '--disable-gpu-process-crash-limit',
        // '--disable-gpu-process-for-dx12-info-collection',
        // '--disable-gpu-program-cache',
        // '--disable-gpu-rasterization',
        // '--disable-gpu-sandbox',
        // '--disable-gpu-shader-disk-cache',
        // '--disable-gpu-vsync',
        // '--disable-gpu-watchdog',
        '--enable-gpu',
        // '--enable-gpu-benchmarking',
        // '--enable-gpu-blocked-time',
        // '--enable-gpu-client-logging',
        // '--enable-gpu-client-tracing',
        // '--enable-gpu-command-logging',
        // '--enable-gpu-debugging',
        // '--enable-gpu-driver-debug-logging',
        // '--enable-gpu-memory-buffer-compositor-resources',
        // '--enable-gpu-memory-buffer-video-frames',
        // '--enable-gpu-rasterization',
        '--ignore-gpu-blocklist',
        '--disable-client-side-phishing-detection',
        '--no-default-browser-check',
        '--enable-automation',
        '--password-store=basic',
        '--disable-sync',
        '--disable-features=OptimizationHints',
        '--hide-scrollbars',
        '--disable-canvas-aa',
        '--disable-2d-canvas-clip-aa',
        '--disable-infobars',
        '--disable-breakpad',
        // '--userDataDir: "/tmp/userDataDir"',
        '--disable-backgrounding-occluded-windows',
        '--disable-hang-monitor',
        '--disable-background-timer-throttling',
        '--disable-chrome-tracing-computation',
        '--disable-component-update',
        '--disable-default-apps',
        '--disable-domain-reliability',
        '--disable-extensions',
        '--disable-ipc-flooding-protection',
        '--disable-notifications',
        '--disable-popup-blocking',
        '--disable-prompt-on-repost',
        '--disable-renderer-accessibility',
        '--disable-renderer-backgrounding',
        '--disable-remote-fonts',
        '--disable-plugins',
        '--disable-remote-playback-api',
        '--disable-audio-input',
        '--disable-gl-drawing-for-tests',
        '--disable-audio-output',
        '--browser-test',
        '--disable-demo-mode',
        '--disable-dinosaur-easter-egg',
        '--disable-domain-blocking-for-3d-apis',
        '--disable-domain-reliability',
        '--disable-software-rasterizer',
        '--disable-background-tasks',
        // '--use-gl=egl',
        '--system-aec-enabled',
    ];
    if (DEBUG) {
        console.log(`DEBUG is set to ${DEBUG}, going to open debugging port on chrome.`);
        argsArray.push(`--remote-debugging-port=${DEBUG_PORT}`);
    }
    return await puppeteer.launch({
        headless: 'new',
        ignoreHTTPSErrors: true,
        dumpio: false,
        args: argsArray,
        ignoreDefaultArgs: ['--mute-audio'],
    });
};

const launchBrowsersGradually = async (count) => {
    let browsers = [];
    for (let i = 0; i < count; i++) {
        console.log(`opening browser num ${i + 1}`);
        const browser = await launchBrowser();
        browsers.push(browser);
        await delay();
    }
    return browsers;
}

const openContextsGradually = async (browsers) => {
    let contexts = [];
    for (let i = 0; i < browsers.length; i++) {
        console.log(`opening context num ${i + 1}`);
        const context = await browsers[i].createIncognitoBrowserContext();
        contexts.push(context);
        await delay();
    }
    return contexts;
}

const openTabsGradually = async (contexts) => {
    let pages = [];
    for (let i = 0; i < contexts.length; i++) {
        console.log(`opening page num ${i + 1}`);
        const context = await contexts[i].newPage();
        pages.push(context);
        await delay();
    }
    return pages;
}

const openNumberOfContextsInBrowser = async (browser, numberOfContexts) => {
    let contexts = [];
    for (let i = 0; i < numberOfContexts; i++) {
        console.log(`opening context num ${i + 1}`);
        const context = await browser.createIncognitoBrowserContext();
        contexts.push(context);
        await delay();
    }
    return contexts;
}

const closeBrowsersDefaultPages = async (browsers) => {
    for (let i = 0; i < browsers.length; i++){
        const pages = await browsers[i].pages();
        await pages[0].close();
    }
}

const turnOffTimeout = (page) => {
    page.setDefaultNavigationTimeout(0);
}

const navigateToUrl = async (page, url) => {
    await page.goto(url, { waitUntil: 'networkidle2' });
    await delay();
}

const reloadPage = async (page) => {
    await page.reload({ waitUntil: 'networkidle2' });
    await delay();
}

const attachLogger = (page, userId, user) => {
    page.on('console', async msg => {
        const args = msg.args();
        args.forEach(async (arg) => {
            try {
                //slice is to remove "JSHandle:" prefix
                let line = arg.toString().slice(9);
                if (line.includes('@object') || line.includes('@array') || line.includes('ms :0') || line.includes('undefined')) {
                    return;
                }
                console.log(`${user}.${userId}: ${line}`);
            } catch (e) {
                console.log(`${user}.${userId}: console.log failed with error: ${e}`);
            }
        })
    });
};

module.exports = {
    launchBrowsersGradually: launchBrowsersGradually,
    openContextsGradually: openContextsGradually,
    closeBrowsersDefaultPages: closeBrowsersDefaultPages,
    openTabsGradually: openTabsGradually,
    turnOffTimeout: turnOffTimeout,
    navigateToUrl: navigateToUrl,
    reloadPage: reloadPage,
    attachLogger: attachLogger,
    launchBrowser: launchBrowser,
    openNumberOfContextsInBrowser: openNumberOfContextsInBrowser,
}