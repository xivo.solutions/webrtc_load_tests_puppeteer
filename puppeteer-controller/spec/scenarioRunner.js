const {
    tearDownEverything,
    executeScenario,
} = require('../libs/scenarioManager');

const {
    showLoadtestReport,
    evaluateScenario
} = require('../libs/callEvaluator');

const {
    setIterationTimeout,
    setCurrentIteration,
    getScenarioName,
    getScenarioDescription,
    delay,
} = require('../libs/utils');

const SCENARIO_NAME = getScenarioName();
const SCENARIO_DESCRIPTION = getScenarioDescription();
const WEBRTC_LOADTEST_TIMEOUT_MS = setIterationTimeout();
const WEBRTC_LOADTEST_ITERATIONS = process.env.WEBRTC_LOADTEST_ITERATIONS;

console.log(`Puppeteer: WEBRTC_LOADTEST_TIMEOUT_MS is set to: ${WEBRTC_LOADTEST_TIMEOUT_MS}`);

describe(SCENARIO_NAME, () => {
    beforeAll(async () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = WEBRTC_LOADTEST_TIMEOUT_MS;
    }, WEBRTC_LOADTEST_TIMEOUT_MS);

    afterAll(async () => {
        await tearDownEverything();
    })

    for (let iteration = 0; iteration < WEBRTC_LOADTEST_ITERATIONS; iteration++) {
        it(SCENARIO_DESCRIPTION, async () => {
            await delay();
            setCurrentIteration(iteration);
            await executeScenario();
            await evaluateScenario();
            showLoadtestReport();
        });
    }
});
