# webrtc_load_test_puppeteer

This project is used for webrtc load testing of XiVO platform.

## description

The docker container contains test runner (jasmine) and browser automation (puppeteer).
In each spec one browser per webrtc user is opened. After all browsers are opened, each user
loads page containing javascript for eastablishing an webtrc call.
At the end webrtc calls are evaluated by checking if correct phone events have happend.

## upgrade

The bundled chromium browser, which is a puppeteer dependency, is downloaded in advance and  
included in docker image xivoxc/alpine-puppeteer. Downloading of this chromium binary is skipped
during `npm install` using env var PUPPETEER_SKIP_CHROMIUM_DOWNLOAD, and instead it's copied into  
`node_modules` to avoid downloading of the browser each time the container is build.

With newer versions of puppeteer, the predownloaded chromium binary should be also upgraded.
To find out which version of chromium does puppeteer use currently, look for `chromium` entry in
`revisions.ts`.
Downloading chromium with the corresponding revision number can be done from the [common data storage of google](https://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html?prefix=Linux_x64/).

## debugging container

In order to debug the container alone you should comment the ENTRYPOINT directive in Dockerfile and under it add these lines:
`RUN ./setup.sh`
`ENTRYPOINT tail -f /dev/null`